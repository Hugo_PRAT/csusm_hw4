import 'package:flutter/material.dart';

class Animate extends AnimatedWidget {

  static final _sizeTween = Tween<double>(begin: 1, end: 200);

  Animate({Key key, Animation<double> animation})
    : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;

    return Center(child: Container(
      margin: EdgeInsets.all(20),
      height: _sizeTween.evaluate(animation),
      width: _sizeTween.evaluate(animation),
      child: FlutterLogo(),
    ));
  }

}